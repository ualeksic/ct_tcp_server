﻿/*
   Copyright 2011 CrypTool 2 Team <ct2contact@cryptool.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
using System.ComponentModel;
using System.Windows.Controls;
using Cryptool.PluginBase;
using Cryptool.PluginBase.Miscellaneous;
using System.Net;
using System.Net.Sockets;
using System;
using System.Text;
using System.Threading;

namespace Cryptool.Plugins.TCP_Server
{
    // HOWTO: Change author name, email address, organization and URL.
    [Author("Singidunum University", "rc@singidunum.ac.rs", "Racunski centar", "http://dir.singidunum.ac.rs")]
    // HOWTO: Change plugin caption (title to appear in CT2) and tooltip.
    // You can (and should) provide a user documentation as XML file and an own icon.
    [PluginInfo("TCP Server", "Server", "TCP_Server/DetailedDescription/doc.xml", new[] { "TCP Server/icon.png" })]
    // HOWTO: Change category to one that fits to your plugin. Multiple categories are allowed.
    [ComponentCategory(ComponentCategory.Protocols)]
    public class TCP_Server : ICrypComponent
    {
        #region Private Variables

        // HOWTO: You need to adapt the settings class as well, see the corresponding file.
        private readonly ExamplePluginCT2Settings settings = new ExamplePluginCT2Settings();
        private TcpListener nodeServer;
        private TcpClient nodeClient = new TcpClient();
        private NetworkStream nodeStream;
        private byte[] nodeBuffer;



        #endregion
        /// <summary>
        /// HOWTO: Input interface to read the input data. 
        /// You can add more input properties of other type if needed.
        /// </summary>

        #region IPlugin Members
        /// <summary>
        /// HOWTO: Output interface to write the output data.
        /// You can add more output properties ot other type if needed.
        /// </summary>
        [PropertyInfo(Direction.OutputData, "Message", "Recived Message [Byte]")]
        public byte[] recivedMsgByte
        {
            get;
            set;
        }
        #endregion


        #region IPlugin Members
        /// <summary>
        /// HOWTO: Output interface to write the output data.
        /// You can add more output properties ot other type if needed.
        /// </summary>
        [PropertyInfo(Direction.InputData, "Response", "Response Message [Byte]")]
        public byte[] responseMsgByte
        {
            get;
            set;
        }
        #endregion


        #region IPlugin Members
        /// <summary>
        /// HOWTO: Output interface to write the output data.
        /// You can add more output properties ot other type if needed.
        /// </summary>
        [PropertyInfo(Direction.OutputData, "Status", "Message Status")]
        public bool msgStatus
        {
            get;
            set;
        }
        #endregion

        #region IPlugin Members

        /// <summary>
        /// Provide plugin-related parameters (per instance) or return null.
        /// </summary>
        public ISettings Settings
        {
            get { return settings; }
        }

        /// <summary>
        /// Provide custom presentation to visualize the execution or return null.
        /// </summary>
        public UserControl Presentation
        {
            get { return null; }
        }

        /// <summary>
        /// Called once when workflow execution starts.
        /// </summary>
        public void PreExecution()
        {
        }

        /// <summary>
        /// Called every time this plugin is run in the workflow execution.
        /// </summary>
        public void Execute()
        {
            ProgressChanged(0, 0);
            if (settings.Ip == "")
            {
                GuiLogMessage("Invalid IP!", NotificationLevel.Error);
            }
            this.InitializeServer();
            this.startServer();
            this.Handle();
            ProgressChanged(1, 1);
            
            
        }

        #region TCP Server Implementation

        public void InitializeServer()
        {
            nodeServer = new TcpListener(IPAddress.Parse(settings.Ip), settings.Port);
            nodeClient = new TcpClient();
        }

        #endregion  

        #region Start TCP Server

        public void startServer()
        {
            nodeServer.Start();
            nodeClient = nodeServer.AcceptTcpClient();
            GuiLogMessage("Starting server at " + settings.Ip + ":" + settings.Port, NotificationLevel.Info);
        }

        #endregion

        #region Handle TCP Server

        public void Handle()
        {
            nodeStream = nodeClient.GetStream();
            nodeBuffer = new byte[(int)nodeClient.ReceiveBufferSize];
            nodeStream.Read(nodeBuffer, 0, (int)nodeClient.ReceiveBufferSize);
            recivedMsgByte = Encoding.ASCII.GetBytes(Encoding.ASCII.GetString(nodeBuffer).Replace("\0", String.Empty));
            if (recivedMsgByte != null)
            {
                msgStatus = true;
                OnPropertyChanged("msgStatus");
            }
            else
            {
                msgStatus = false;
                OnPropertyChanged("msgStatus");
            }
            OnPropertyChanged("recivedMsgByte");
            nodeStream.Flush();
            if (responseMsgByte != null)
            {
                nodeStream.Write(responseMsgByte, 0, responseMsgByte.Length);
                nodeStream.Flush();
            }
        }

        #endregion

        #region Stop TCP Server

        public void stopServer() {
            nodeClient.Close();
            nodeServer.Stop();
            GuiLogMessage("Server shutting down.", NotificationLevel.Info);
        }

        #endregion
        /// <summary>
        /// Called once after workflow execution has stopped.
        /// </summary>
        public void PostExecution()
        {
        }

        /// <summary>
        /// Triggered time when user clicks stop button.
        /// Shall abort long-running execution.
        /// </summary>
        public void Stop()
        {
            this.stopServer();
        }

        /// <summary>
        /// Called once when plugin is loaded into editor workspace.
        /// </summary>
        public void Initialize()
        {
            IPAddress[] ipAddrs = Dns.GetHostByName(Dns.GetHostName()).AddressList;
            GuiLogMessage("Available local IPs: ", NotificationLevel.Info);
            for (int i = 0; i < ipAddrs.Length; i++)
            {
                GuiLogMessage("\t["+(i+1)+"] "+ipAddrs[i],NotificationLevel.Info);
            }
        }

        /// <summary>
        /// Called once when plugin is removed from editor workspace.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion

        #region Event Handling

        public event StatusChangedEventHandler OnPluginStatusChanged;

        public event GuiLogNotificationEventHandler OnGuiLogNotificationOccured;

        public event PluginProgressChangedEventHandler OnPluginProgressChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        private void GuiLogMessage(string message, NotificationLevel logLevel)
        {
            EventsHelper.GuiLogMessage(OnGuiLogNotificationOccured, this, new GuiLogEventArgs(message, this, logLevel));
        }

        private void OnPropertyChanged(string name)
        {
            EventsHelper.PropertyChanged(PropertyChanged, this, new PropertyChangedEventArgs(name));
        }

        private void ProgressChanged(double value, double max)
        {
            EventsHelper.ProgressChanged(OnPluginProgressChanged, this, new PluginProgressEventArgs(value, max));
        }

        #endregion
    }
}
